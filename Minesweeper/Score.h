#pragma once
#pragma warning(disable:4996)
#include <fstream>
#include <tuple>
#include <ios>

class Score
{
private:
	std::string m_scoreFilePath;
	std::tuple<std::string, std::string, int> m_score;

	bool isEmpty(const std::string& path);

public:
	Score(const std::string& filePath);

	const std::tuple<std::string, std::string, int> getScore() const;
	void setScore(std::tuple<std::string, std::string, int> score);
	void writeScore();
};