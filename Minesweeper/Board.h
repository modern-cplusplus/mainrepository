#pragma once
#include "Cell.h"
#include <vector>
#include <set>

class Board
{
private:
	std::vector<std::vector<Cell>> m_board;

	static constexpr size_t default_row_min = 1;
	static constexpr size_t default_column_min = 8;
	static constexpr size_t default_mineNumber = 1;
	static constexpr size_t default_row_max = 25;
	static constexpr size_t default_column_max = 32;

	size_t m_row;
	size_t m_column;
	size_t m_mineNumber;
	size_t m_cellsRevealed;
	short int m_flagCounter;

	std::set<std::pair<size_t, size_t>> m_modifiedCells;
	std::set<std::pair<size_t, size_t>> m_mineCoordinates;
	std::set<std::pair<size_t, size_t>> m_wrongFlagsCoordinates;
	std::set<std::pair<size_t, size_t>> m_unflaggedMines;

	void generateStartingBoard();
	void deployMines(const size_t& clickedRow, const size_t& clickedCol);
	void markNumberMineNeighbors();

public:
	Board();
	Board(const size_t& row, const size_t& column, const size_t& mineNumber);

	std::vector<std::vector<Cell>> getBoard() const;
	const size_t getRow() const;
	const size_t getColumn() const;
	const size_t getMineNumber() const;
	const size_t getCellsRevealed() const;
	const short int getFlagCounter()const;
	const std::set<std::pair<size_t, size_t>> getModifiedCells() const;

	void setFlagCounter(short int flagCounter);

	void generateBoardAfterClick(const size_t& clickedRow, const size_t& clickedCol);
	void insertModifiedCells(std::pair<size_t, size_t> coordinates);
	void clearModifiedCells();

	const bool validateDifficulty(const size_t& row, const size_t& col, const size_t& mine_num) const;

	void fullMineReveal(size_t clickedRow, size_t clickedCol);
	void fullMineFlag();

	void clickCell(const size_t& clickedRow, const size_t& clickedCol);

	void doFlagging(const size_t& clickedRow, const size_t& clickedCol);
	void doPressing(const size_t& clickedRow, const size_t& clickedCol);
	void doUnpressing(const size_t& clickedRow, const size_t& clickedCol);

	~Board();
};