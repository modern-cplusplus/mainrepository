#include "TileMap.h"

bool TileMap::load(const std::string& tileset,
	sf::Vector2u tileSize,
	const std::vector<std::unique_ptr<int>> tiles,
	size_t column,
	size_t row,
	std::set<std::pair<size_t, size_t>> modifiedCells)
{
	if (!m_tileset.loadFromFile(tileset))
	{
		return false;
	}

	m_vertices.setPrimitiveType(sf::Quads);
	m_vertices.resize(column * row * 4);

	for (const auto& iterator : modifiedCells)
	{
		int tileNumber = *tiles.at(iterator.second + iterator.first * column);

		int tilesetPositionRow = tileNumber % (m_tileset.getSize().x / tileSize.x);
		int tilesetPositionColumn = tileNumber / (m_tileset.getSize().x / tileSize.x);

		sf::Vertex* corners = &m_vertices[(iterator.second + iterator.first * column) * 4];

		corners[0].position = sf::Vector2f((float)iterator.second * tileSize.x, (float)iterator.first * tileSize.y);
		corners[1].position = sf::Vector2f((float)(iterator.second + 1) * tileSize.x, (float)iterator.first * tileSize.y);
		corners[2].position = sf::Vector2f((float)(iterator.second + 1) * tileSize.x, (float)(iterator.first + 1) * tileSize.y);
		corners[3].position = sf::Vector2f((float)iterator.second * tileSize.x, (float)(iterator.first + 1) * tileSize.y);

		corners[0].texCoords = sf::Vector2f((float)tilesetPositionRow * tileSize.x, (float)tilesetPositionColumn * tileSize.y);
		corners[1].texCoords = sf::Vector2f((float)(tilesetPositionRow + 1) * tileSize.x, (float)tilesetPositionColumn * tileSize.y);
		corners[2].texCoords = sf::Vector2f((float)(tilesetPositionRow + 1) * tileSize.x, (float)(tilesetPositionColumn + 1) * tileSize.y);
		corners[3].texCoords = sf::Vector2f((float)tilesetPositionRow * tileSize.x, (float)(tilesetPositionColumn + 1) * tileSize.y);
	}

	return true;
}

void TileMap::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = &m_tileset;
	target.draw(m_vertices, states);
}