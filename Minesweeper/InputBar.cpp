#include "InputBar.h"

InputBar::InputBar(float sizeX, float sizeY, float coordX, float coordY, sf::String startingInput) :
	m_sizeY(sizeY),
	m_sizeX(sizeX),
	m_coordX(coordX),
	m_coordY(coordY),
	m_input(startingInput),
	m_startingInput(startingInput)
{
	m_font.loadFromFile("fonts\\DigifaceRegular.ttf");

	m_inputShape.setSize(sf::Vector2f(m_sizeX, m_sizeY));
	m_inputShape.setPosition(m_coordX, m_coordY);
	m_inputText.setPosition(m_coordX, m_coordY);

	m_inputText.setFillColor(sf::Color::Black);
	m_inputText.setCharacterSize(20);
	m_inputText.setString(m_input);
	m_inputText.setFont(m_font);
}

sf::Text& InputBar::getInputText()
{
	return m_inputText;
}

const size_t InputBar::getInput() const
{
	if (m_addedNum.getSize() == 0 || !isAddedNumCorrect())
	{
		return 0;
	}
	std::string aux = m_addedNum;

	return std::stoi(aux);
}

const sf::RectangleShape& InputBar::getInputShape() const
{
	return m_inputShape;
}

void InputBar::setInputText(sf::Window& window, sf::Event& event, const size_t& inputLength)
{
	sf::Vector2i mousePosition = sf::Mouse::getPosition(window);
	bool selected = mouseOverBox(window);
	if (selected)
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
		{
			if (m_addedNum.getSize() && m_input.getSize())
			{
				m_addedNum.erase(m_addedNum.getSize() - 1, m_addedNum.getSize());
				m_input.erase(m_input.getSize() - 1, m_input.getSize());
			}
		}
		else if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Enter) && m_addedNum.getSize() < inputLength)
		{
			m_input += event.text.unicode;
			m_addedNum += event.text.unicode;
		}

		m_inputText.setString(m_input);
	}
}

void InputBar::setInputText(const size_t& input)
{
	m_input = m_startingInput;
	m_addedNum = "";
	std::string stringInput = std::to_string(input);
	m_addedNum += stringInput;
	m_input += stringInput;
	m_inputText.setString(m_input);
}

bool InputBar::mouseOverBox(sf::Window& window)
{
	sf::Vector2i mousePos = sf::Mouse::getPosition(window);
	if (mousePos.x >= m_inputShape.getPosition().x && mousePos.x <= m_inputShape.getPosition().x +
		m_sizeX && mousePos.y >= m_inputShape.getPosition().y && mousePos.y <= m_inputShape.getPosition().y + m_sizeY && sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		m_inputText.setFillColor(sf::Color::Red);
		return true;
	}
	else if ((mousePos.x < m_inputShape.getPosition().x || mousePos.x > m_inputShape.getPosition().x +
		m_sizeX || mousePos.y < m_inputShape.getPosition().y || mousePos.y > m_inputShape.getPosition().y) + m_sizeY && sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		m_inputText.setFillColor(sf::Color::Black);
		return false;
	}
	else if (m_inputText.getFillColor() == sf::Color::Red) { return true; }

	return false;
}

const bool InputBar::isAddedNumCorrect() const
{
	for (unsigned int index = 0; index < m_addedNum.getSize(); ++index)
	{
		if (!isdigit(m_addedNum[index]))
		{
			return false;
		}
	}

	return true;
}