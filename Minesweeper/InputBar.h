#pragma once
#include <SFML/Graphics.hpp>

class InputBar
{
private:
	const sf::String m_startingInput;
	sf::String m_input;
	sf::String m_addedNum;
	sf::Text m_inputText;
	sf::Font m_font;

	sf::RectangleShape m_inputShape;

	float m_sizeX;
	float m_sizeY;
	float m_coordX;
	float m_coordY;

	const bool isAddedNumCorrect() const;

public:
	InputBar(float xSize, float ySize, float coordX, float coordY, sf::String startingInput);

	sf::Text& getInputText();
	const size_t getInput() const;
	const sf::RectangleShape& getInputShape() const;

	void setInputText(sf::Window& window, sf::Event& event, const size_t& inputLength);
	void setInputText(const size_t& input);

	bool mouseOverBox(sf::Window& window);
};