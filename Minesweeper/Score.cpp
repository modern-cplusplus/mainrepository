#include "Score.h"
#include <iomanip>
#include <chrono>
#include <sstream>

Score::Score(const std::string& filePath)
{
	m_scoreFilePath = filePath;
}

const std::tuple<std::string, std::string, int> Score::getScore() const
{
	return m_score;
}

void Score::setScore(std::tuple<std::string, std::string, int> score)
{
	m_score = score;
}

void Score::writeScore()
{
	std::ofstream outputFile(m_scoreFilePath, std::ios_base::app);
	if (isEmpty(m_scoreFilePath))
	{
		outputFile << "  PLAYER   |        DIFFICULTY        | TIME RECORD |           DATE          ";
	}

	outputFile << "\n-----------+--------------------------+-------------+-------------------------";

	auto clockNow = std::chrono::system_clock::now();
	std::time_t dateTimeNow = std::time(nullptr);
	std::stringstream currentTime;
	currentTime << std::put_time(std::localtime(&dateTimeNow), "%Y-%m-%d %H:%M:%S");
	std::string currentTime_string = currentTime.str();

	outputFile << "\n" << std::get<0>(m_score) << std::setfill(' ') << std::setw(13 - std::get<0>(m_score).size()) << " | "
		<< std::get<1>(m_score) << std::setfill(' ') << std::setw(27 - std::get<1>(m_score).size()) << " | "
		<< std::get<2>(m_score) << std::setfill(' ') << std::setw((std::streamsize)(14 - (std::get<2>(m_score) > 1 ? log10(std::get<2>(m_score)) : 1))) << " | "
		<< currentTime_string;
}

bool Score::isEmpty(const std::string& path)
{
	std::ifstream tempFile(path);
	return tempFile.peek() == std::ifstream::traits_type::eof();
}