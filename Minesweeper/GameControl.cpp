#include "GameControl.h"
#include <SFML/Audio.hpp>
#include <thread>
#include <chrono>

GameControl::GameControl()
{
	isSfxPlayed = false;
	isSfxActive = true;
	firstClick = false;
	gameOver = false;
	winner = false;
	enteredName = false;

	m_difficultyName = "H: " + std::to_string(m_board.getRow());
	for (auto row = 3; row > log10(m_board.getRow()) + 1; --row) { m_difficultyName += ' '; }
	m_difficultyName += "  W: " + std::to_string(m_board.getColumn());
	for (auto col = 3; col > log10(m_board.getColumn()) + 1; --col) { m_difficultyName += ' '; }
	m_difficultyName += "  M: " + std::to_string(m_board.getMineNumber());
	for (auto mine = 3; mine > log10(m_board.getMineNumber()) + 1; --mine) { m_difficultyName += ' '; }

	tilesetInitialize();
}

void GameControl::runInitialize(sf::RenderWindow& window)
{
	window.setFramerateLimit(60);

	sf::Image icon;
	icon.loadFromFile("sprites/icon.png");
	window.setIcon(16, 16, icon.getPixelsPtr());

	tilesetIndexing();
	m_map.setScale(2, 2);
	m_map.setPosition(0, ratio * 4);

	firstClick = true;
}

void GameControl::run()
{
	windowWidth = m_board.getColumn() * ratio;
	windowHeight = (m_board.getRow() + 4) * ratio;

	sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "Minesweeper++", sf::Style::Titlebar | sf::Style::Close);

	runInitialize(window);

	while (window.isOpen())
	{
		eventHandling(window);

		setSmiley("sprites/smiley_default.png");
		setSound();
		setCustomBoard();

		window.clear(sf::Color(192, 192, 192));

		if (m_board.getCellsRevealed() == 0)
		{
			if (!gameOver)
			{
				m_board.fullMineFlag();
				tilesetIndexing();
			}

			gameOver = true;
			winner = true;

			m_board.setFlagCounter(0);
			showFlagCounter(window);
		}

		if (gameOver)
		{
			if (!isSfxPlayed) { soundGameOver(window, winner); }

			if (!winner)
			{
				setSmiley("sprites/smiley_dead.png");
			}
			else
			{
				setSmiley("sprites/smiley_win.png");

				if (!enteredName)
				{
					setUsername(window);
					scoreThread((int)elapsed.asSeconds());
					enteredName = true;
				}
			}
		}

		paintGameComponents(window);

		window.display();
	}
}

void GameControl::tilesetInitialize()
{
	for (size_t index = 0; index < m_board.getRow() * m_board.getColumn(); ++index)
	{
		m_cellTilesetValues.emplace_back(new int(0));
	}
}

void GameControl::tilesetIndexing()
{
	tilesetInitialize();

	std::set<std::pair<size_t, size_t>> modifiedCells = m_board.getModifiedCells();
	for (const auto& itr : modifiedCells)
	{
		if (m_board.getBoard()[itr.first][itr.second].isHidden())
		{
			if (m_board.getBoard()[itr.first][itr.second].isPressed())
			{
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(1);
			}
			else
			{
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(0);
			}
		}
		else if (m_board.getBoard()[itr.first][itr.second].isFlagged())
		{
			m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(10);
		}
		else
		{
			switch (m_board.getBoard()[itr.first][itr.second].getState())
			{
			case Cell::CellState::MINE:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(11);
				break;

			case Cell::CellState::MINE_CLICKED:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(12);
				gameOver = true;
				break;

			case Cell::CellState::MINE_CROSSED:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(13);
				break;

			case Cell::CellState::EMPTY:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(1);
				break;

			case Cell::CellState::NUMBER_1:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(2);
				break;

			case Cell::CellState::NUMBER_2:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(3);
				break;

			case Cell::CellState::NUMBER_3:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(4);
				break;

			case Cell::CellState::NUMBER_4:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(5);
				break;

			case Cell::CellState::NUMBER_5:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(6);
				break;

			case Cell::CellState::NUMBER_6:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(7);
				break;

			case Cell::CellState::NUMBER_7:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(8);
				break;

			case Cell::CellState::NUMBER_8:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(9);
				break;

			default:
				m_cellTilesetValues[itr.second + itr.first * m_board.getColumn()] = std::make_unique<int>(-1);
				break;
			}
		}
	}

	if (!m_map.load("sprites/cell_tileset.png", sf::Vector2u(ratio / 2, ratio / 2), std::move(m_cellTilesetValues), m_board.getColumn(), m_board.getRow(), m_board.getModifiedCells()))
	{
		return;
	}

	m_board.clearModifiedCells();
}

void GameControl::eventHandling(sf::RenderWindow& window)
{
	sf::Event event;
	sf::Mouse mouse;

	while (window.pollEvent(event))
	{
		if ((event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) || (event.type == sf::Event::Closed))
		{
			window.close();
		}

		if (event.type == sf::Event::Resized)
		{
			sf::FloatRect resetView(0, 0, (float)windowWidth, (float)windowHeight);
			window.setView(sf::View(resetView));
		}

		if (mouse.isButtonPressed(mouse.Left) && event.key.code == mouse.Left)
		{
			if (isContainedHUD(spriteSmiley.getPosition(), window, mouse, ratio / 2))
			{
				while (mouse.isButtonPressed(mouse.Left))
				{
					window.clear(sf::Color(192, 192, 192));

					if (isContainedHUD(spriteSmiley.getPosition(), window, mouse, ratio / 2))
					{
						setSmiley("sprites/smiley_clicked.png");
					}
					else if (gameOver)
					{
						if (!winner) { setSmiley("sprites/smiley_dead.png"); }
						else { setSmiley("sprites/smiley_win.png"); }
					}
					else { setSmiley("sprites/smiley_default.png"); }

					tilesetIndexing();

					paintGameComponents(window);

					window.display();
				}

				if (isContainedHUD(spriteSmiley.getPosition(), window, mouse, ratio / 2))
				{
					m_board = Board(m_board.getRow(), m_board.getColumn(), m_board.getMineNumber());
					resetGame();
					return;
				}
			}
			else if (isContainedHUD(spriteSound.getPosition(), window, mouse, ratio / 8))
			{
				m_sound.setVolume(isSfxActive);

				if (isSfxActive) { isSfxActive = false; }
				else { isSfxActive = true; }
			}
			else if (isContainedHUD(spriteSize.getPosition(), window, mouse, ratio / 8))
			{
				settingsWindow(window);
			}
		}

		if (!gameOver)
		{
			if (mouse.isButtonPressed(mouse.Right) && event.key.code == mouse.Right)
			{
				if (mouse.getPosition(window).x >= 0 && mouse.getPosition(window).y - ratio * 4 >= 0 &&
					isContainedCell(window, mouse))
				{
					if (m_board.getBoard()[(mouse.getPosition(window).y - ratio * 4) / ratio][mouse.getPosition(window).x / ratio].isHidden() ||
						m_board.getBoard()[(mouse.getPosition(window).y - ratio * 4) / ratio][mouse.getPosition(window).x / ratio].isFlagged())
					{
						m_sound.playPlaceFlag();
						m_board.doFlagging((mouse.getPosition(window).y - ratio * 4) / ratio, mouse.getPosition(window).x / ratio);
						tilesetIndexing();
						return;
					}
				}
			}
			else if (mouse.isButtonPressed(mouse.Left) && event.key.code == mouse.Left)
			{
				if (mouse.getPosition(window).y - ratio * 4 > 0 && mouse.getPosition(window).x > 0)
				{
					size_t oldRow = (mouse.getPosition(window).y - ratio * 4) / ratio;
					size_t oldCol = mouse.getPosition(window).x / ratio;

					if (oldCol >= 0 && oldRow >= 0 && oldCol < m_board.getColumn() && oldRow < m_board.getRow())
					{
						if (m_board.getBoard()[oldRow][oldCol].isHidden())
						{
							setSmiley("sprites/smiley_surprised.png");

							m_sound.playClickCell();

							while (mouse.isButtonPressed(mouse.Left))
							{
								window.clear(sf::Color(192, 192, 192));
								if (mouse.getPosition(window).x >= 0 && mouse.getPosition(window).y - ratio * 4 >= 0 &&
									isContainedCell(window, mouse))
								{
									if (oldRow != (mouse.getPosition(window).y - ratio * 4) / ratio || oldCol != mouse.getPosition(window).x / ratio)
									{
										m_board.doUnpressing(oldRow, oldCol);
										m_board.insertModifiedCells({ oldRow, oldCol });
									}

									if (isContainedCell(window, mouse))
									{
										m_board.doUnpressing(oldRow, oldCol);
										m_board.insertModifiedCells({ oldRow, oldCol });

										oldRow = (mouse.getPosition(window).y - ratio * 4) / ratio;
										oldCol = mouse.getPosition(window).x / ratio;
										if (!m_board.getBoard()[oldRow][oldCol].isPressed() && !m_board.getBoard()[oldRow][oldCol].isFlagged())
										{
											m_board.doPressing(oldRow, oldCol);
											m_board.insertModifiedCells({ oldRow, oldCol });
										}
									}
									else
									{
										m_board.doUnpressing(oldRow, oldCol);

										m_board.insertModifiedCells({ oldRow, oldCol });
										tilesetIndexing();
									}
								}
								else if (m_board.getBoard()[oldRow][oldCol].isPressed())
								{
									m_board.doUnpressing(oldRow, oldCol);
									m_board.insertModifiedCells({ oldRow, oldCol });
								}
								tilesetIndexing();

								paintGameComponents(window);

								window.display();
							}

							if (mouse.getPosition(window).x < 0 && mouse.getPosition(window).y - ratio * 4 < 0 &&
								!isContainedCell(window, mouse))
							{
								m_board.doUnpressing(oldRow, oldCol);
								m_board.insertModifiedCells({ oldRow, oldCol });
								tilesetIndexing();
								return;
							}

							if (firstClick && isContainedCell(window, mouse))
							{
								m_board.generateBoardAfterClick(oldRow, oldCol);
								firstClick = false;
								sf::Clock startClock;
								clock = startClock;
								tilesetIndexing();
							}
							if (m_board.getBoard()[oldRow][oldCol].isHidden() && isContainedCell(window, mouse))
							{
								m_board.clickCell(oldRow, oldCol);
								tilesetIndexing();
							}
						}
					}
				}
			}
			else if (mouse.isButtonPressed(mouse.Middle) && event.key.code == mouse.Middle)
			{
				if (mouse.getPosition(window).y - ratio * 4 > 0 && mouse.getPosition(window).x > 0)
				{
					size_t oldRow = (mouse.getPosition(window).y - ratio * 4) / ratio;
					size_t oldCol = mouse.getPosition(window).x / ratio;

					std::set<std::pair<int, int>> oldPressedNeighbours;
					std::set<std::pair<int, int>> newPressedNeighbours;

					for (short int rowOff = -1; rowOff <= 1; ++rowOff)
					{
						for (short int colOff = -1; colOff <= 1; ++colOff)
						{
							short int currentRow = (short int)oldRow + rowOff;
							short int currentCol = (short int)oldCol + colOff;

							if (currentRow > -1 && currentRow < (short int)m_board.getRow() && currentCol > -1 && currentCol < (short int)m_board.getColumn())
							{
								m_board.doPressing(currentRow, currentCol);
								oldPressedNeighbours.insert({ currentRow, currentCol });
								m_board.insertModifiedCells({ currentRow, currentCol });
							}
						}
					}

					if (oldCol >= 0 && oldRow >= 0 && oldCol < m_board.getColumn() && oldRow < m_board.getRow())
					{
						setSmiley("sprites/smiley_surprised.png");

						while (mouse.isButtonPressed(mouse.Middle))
						{
							window.clear(sf::Color(192, 192, 192));
							if (mouse.getPosition(window).x >= 0 && mouse.getPosition(window).y - ratio * 4 >= 0 &&
								isContainedCell(window, mouse))
							{
								size_t currentRow = (mouse.getPosition(window).y - ratio * 4) / ratio;
								size_t currentCol = mouse.getPosition(window).x / ratio;

								for (short int rowOff = -1; rowOff <= 1; ++rowOff)
								{
									for (short int colOff = -1; colOff <= 1; ++colOff)
									{
										short int tempRow = (short int)currentRow + rowOff;
										short int tempCol = (short int)currentCol + colOff;

										if (tempRow > -1 && tempRow < (short int)m_board.getRow() && tempCol > -1 && tempCol < (short int)m_board.getColumn())
										{
											m_board.doPressing(tempRow, tempCol);
											newPressedNeighbours.insert({ tempRow, tempCol });
											m_board.insertModifiedCells({ tempRow, tempCol });
										}
									}
								}

								if (oldRow != currentRow || oldCol != currentCol)
								{
									for (const auto& itr : oldPressedNeighbours)
									{
										if (newPressedNeighbours.count(itr) == 0)
										{
											m_board.doUnpressing(itr.first, itr.second);
											m_board.insertModifiedCells({ itr.first, itr.second });
										}
									}

									oldPressedNeighbours = newPressedNeighbours;
									oldRow = currentRow;
									oldCol = currentCol;
								}
							}
							else if (m_board.getBoard()[oldRow][oldCol].isPressed())
							{
								for (const auto& itr : oldPressedNeighbours)
								{
									m_board.doUnpressing(itr.first, itr.second);
									m_board.insertModifiedCells({ itr.first, itr.second });
								}
							}
							tilesetIndexing();
							newPressedNeighbours.clear();

							paintGameComponents(window);

							window.display();
						}

						for (const auto& itr : oldPressedNeighbours)
						{
							m_board.doUnpressing(itr.first, itr.second);
							m_board.insertModifiedCells({ itr.first, itr.second });
						}
						tilesetIndexing();
					}
				}
			}
		}
	}
}

void GameControl::settingsWindow(sf::RenderWindow& window)
{
	sf::Event event;
	sf::Font font;
	sf::Text startButton;
	sf::Text error;
	sf::Text beginnerDifficulty;
	sf::Text intermediateDifficulty;
	sf::Text expertDifficulty;
	sf::Cursor cursor;
	sf::Texture backgroundTexture;
	sf::Sprite background;
	sf::Image icon;
	sf::RenderWindow settings(sf::VideoMode(menuWidth, menuHeight), "Settings", sf::Style::Titlebar | sf::Style::Close);

	icon.loadFromFile("sprites/custom_board.png");
	settings.setIcon(18, 18, icon.getPixelsPtr());
	font.loadFromFile("fonts\\DigifaceRegular.ttf");

	InputBar cellGridY(128, 32, 32, 64, "Height: ");
	InputBar cellGridX(128, 32, 32, 112, "Width: ");
	InputBar mineNumber(128, 32, 32, 160, "Mines: ");

	startButton.setFont(font);
	startButton.setFillColor(sf::Color(255, 60, 0));
	startButton.setOutlineColor(sf::Color::Black);
	startButton.setOutlineThickness(1.5);
	startButton.setCharacterSize(32);
	startButton.setString("OK");
	startButton.setPosition(256.f, 160.f);

	error.setFont(font);
	error.setFillColor(sf::Color::Red);
	error.setCharacterSize(16);
	error.setPosition(192.f, 128.f);

	beginnerDifficulty.setFont(font);
	beginnerDifficulty.setFillColor(sf::Color(150, 255, 0));
	beginnerDifficulty.setOutlineThickness(1.5);
	beginnerDifficulty.setCharacterSize(20);
	beginnerDifficulty.setString("BEGINNER");
	beginnerDifficulty.setPosition(32.f, 16.f);

	intermediateDifficulty.setFont(font);
	intermediateDifficulty.setFillColor(sf::Color(25, 200, 255));
	intermediateDifficulty.setOutlineColor(sf::Color::Black);
	intermediateDifficulty.setOutlineThickness(1.5);
	intermediateDifficulty.setCharacterSize(20);
	intermediateDifficulty.setString("INTERMEDIATE");
	intermediateDifficulty.setPosition(118.f, 16.f);

	expertDifficulty.setFont(font);
	expertDifficulty.setFillColor(sf::Color(255, 60, 0));
	expertDifficulty.setOutlineColor(sf::Color::Black);
	expertDifficulty.setOutlineThickness(1.5);
	expertDifficulty.setCharacterSize(20);
	expertDifficulty.setString("EXPERT");
	expertDifficulty.setPosition(240.f, 16.f);

	while (settings.isOpen())
	{
		while (settings.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				settings.close();
			}
			if (event.type == sf::Event::TextEntered)
			{
				error.setString("");

				cellGridX.setInputText(settings, event, 3);
				cellGridY.setInputText(settings, event, 3);
				mineNumber.setInputText(settings, event, 3);

				if (cellGridX.mouseOverBox(settings) || cellGridY.mouseOverBox(settings) || mineNumber.mouseOverBox(settings))
				{
					m_sound.playPlaceFlag();
				}
			}

			sf::Vector2i mouse = sf::Mouse::getPosition(settings);
			if (cellGridX.getInputText().getGlobalBounds().contains((float)mouse.x, (float)mouse.y) ||
				cellGridY.getInputText().getGlobalBounds().contains((float)mouse.x, (float)mouse.y) ||
				mineNumber.getInputText().getGlobalBounds().contains((float)mouse.x, (float)mouse.y))
			{
				cursor.loadFromSystem(sf::Cursor::Hand);
			}
			else
			{
				cursor.loadFromSystem(sf::Cursor::Arrow);
			}
			settings.setMouseCursor(cursor);

			if (beginnerDifficulty.getGlobalBounds().contains((float)mouse.x, (float)mouse.y))
			{
				beginnerDifficulty.setPosition(28.f, 12.f);
				beginnerDifficulty.setScale(1.1f, 1.1f);
				beginnerDifficulty.setOutlineColor(sf::Color::Red);

				if (event.type == sf::Event::MouseButtonPressed)
				{
					m_sound.playPlaceFlag();

					cellGridY.setInputText(beginnerHeight);
					cellGridX.setInputText(beginnerWidth);
					mineNumber.setInputText(beginnerMineNum);
				}
			}
			else {
				beginnerDifficulty.setPosition(32.f, 16.f);
				beginnerDifficulty.setScale(1, 1);
				beginnerDifficulty.setOutlineColor(sf::Color::Black);
			}

			if (intermediateDifficulty.getGlobalBounds().contains((float)mouse.x, (float)mouse.y))
			{
				intermediateDifficulty.setPosition(112.f, 12.f);
				intermediateDifficulty.setScale(1.1f, 1.1f);
				intermediateDifficulty.setOutlineColor(sf::Color(250, 25, 225));

				if (event.type == sf::Event::MouseButtonPressed)
				{
					m_sound.playPlaceFlag();

					cellGridY.setInputText(intermeditateHeight);
					cellGridX.setInputText(intermediateWidth);
					mineNumber.setInputText(intermediateMineNum);
				}
			}
			else {
				intermediateDifficulty.setPosition(118.f, 16.f);
				intermediateDifficulty.setScale(1, 1);
				intermediateDifficulty.setOutlineColor(sf::Color::Black);
			}

			if (expertDifficulty.getGlobalBounds().contains((float)mouse.x, (float)mouse.y))
			{
				expertDifficulty.setPosition(236.f, 12.f);
				expertDifficulty.setScale(1.1f, 1.1f);
				expertDifficulty.setOutlineColor(sf::Color(0, 25, 255));

				if (event.type == sf::Event::MouseButtonPressed)
				{
					m_sound.playPlaceFlag();

					cellGridY.setInputText(expertHeight);
					cellGridX.setInputText(expertWidth);
					mineNumber.setInputText(expertMineNum);
				}
			}
			else {
				expertDifficulty.setPosition(240.f, 16.f);
				expertDifficulty.setScale(1, 1);
				expertDifficulty.setOutlineColor(sf::Color::Black);
			}

			if (startButton.getGlobalBounds().contains((float)mouse.x, (float)mouse.y))
			{
				startButton.setPosition(254.f, 158.f);
				startButton.setFillColor(sf::Color(150, 255, 0));
				startButton.setScale(1.1f, 1.1f);

				if (event.type == sf::Event::MouseButtonPressed)
				{
					if (m_board.validateDifficulty(cellGridY.getInput(), cellGridX.getInput(), mineNumber.getInput()))
					{
						m_sound.playClickCell();

						m_board = Board(cellGridY.getInput(), cellGridX.getInput(), mineNumber.getInput());
						m_difficultyName = "H: " + std::to_string(cellGridY.getInput());
						for (auto row = 3; row > log10(cellGridY.getInput()) + 1; --row) { m_difficultyName += ' '; }
						m_difficultyName += "  W: " + std::to_string(m_board.getColumn());
						for (auto col = 3; col > log10(cellGridX.getInput()) + 1; --col) { m_difficultyName += ' '; }
						m_difficultyName += "  M: " + std::to_string(m_board.getMineNumber());
						for (auto mine = 3; mine > log10(mineNumber.getInput()) + 1; --mine) { m_difficultyName += ' '; }

						settings.close();

						resetGame();

						windowWidth = m_board.getColumn() * ratio;
						windowHeight = (m_board.getRow() + 4) * ratio;
						window.setSize(sf::Vector2u(windowWidth, windowHeight));
						using namespace std::chrono;
						std::this_thread::sleep_for(0.1s);
					}
					else
					{
						if (cellGridY.getInput() == 4 && cellGridX.getInput() == 5 && mineNumber.getInput() == 0)
						{
							error.setString("Happy Birthday,\nTupi!");
							m_sound.playEasterEgg();
							break;
						}
						if (cellGridY.getInput() == 2 && cellGridX.getInput() == 9 && mineNumber.getInput() == 0)
						{
							error.setString("Happy Birthday,\nFilip!");
							m_sound.playEasterEgg();
							break;
						}
						if (cellGridY.getInput() == 2 && cellGridX.getInput() == 2 && mineNumber.getInput() == 1)
						{
							error.setString("Happy Birthday,\nValent!");
							m_sound.playEasterEgg();
							break;
						}
						if (cellGridY.getInput() == 9 && cellGridX.getInput() == 8 && mineNumber.getInput() == 0)
						{
							error.setString("Happy Birthday,\nMax!");
							m_sound.playEasterEgg();
							break;
						}
						else { error.setString("Invalid Input!"); m_sound.playMineDeploy(); break; }

						settings.draw(error);
						settings.display();
					}
				}
			}
			else {
				startButton.setPosition(256.f, 160.f);
				startButton.setScale(1, 1);
				startButton.setFillColor(sf::Color(255, 60, 0));
			}
		}

		cellGridX.mouseOverBox(settings);
		cellGridY.mouseOverBox(settings);
		mineNumber.mouseOverBox(settings);

		settings.clear(sf::Color::White);
		settings.draw(background);
		settings.draw(cellGridX.getInputText());
		settings.draw(cellGridY.getInputText());
		settings.draw(mineNumber.getInputText());
		settings.draw(startButton);
		settings.draw(error);
		settings.draw(beginnerDifficulty);
		settings.draw(intermediateDifficulty);
		settings.draw(expertDifficulty);
		settings.display();

		window.clear(sf::Color(192, 192, 192));
		paintGameComponents(window);
		window.display();
	}
}

void GameControl::paintGameComponents(sf::RenderWindow& window)
{
	window.draw(m_map);
	window.draw(spriteSmiley);
	window.draw(spriteSound);
	window.draw(spriteSize);

	showFlagCounter(window);
	showTimer(window);
}

void GameControl::resetGame()
{
	tilesetIndexing();
	gameOver = false;
	winner = false;
	firstClick = true;
	enteredName = false;
	isSfxPlayed = false;
	clock.restart();
	elapsed = clock.getElapsedTime();
}

void GameControl::setSmiley(const std::string& filePath)
{
	spriteSmiley.setScale(sf::Vector2f(3, 3));
	spriteSmiley.setPosition(sf::Vector2f(windowWidth / 2.f - 25.f, ratio / 2.f));
	textureSmiley.loadFromFile(filePath);
	spriteSmiley.setTexture(textureSmiley);
}

void GameControl::setCustomBoard()
{
	spriteSize.setScale(sf::Vector2f(2, 2));
	spriteSize.setPosition(sf::Vector2f(windowWidth / 2.f - ratio / 2.f + ratio, ratio * 2.5f));
	textureSize.loadFromFile("sprites/custom_board.png");
	spriteSize.setTexture(textureSize);
}

void GameControl::setSound()
{
	spriteSound.setScale(sf::Vector2f(2, 2));
	spriteSound.setPosition(sf::Vector2f(windowWidth / 2.f - ratio / 2.f - ratio, ratio * 2.5f));
	if (isSfxActive) { textureSound.loadFromFile("sprites/sound_on.png"); }
	else { textureSound.loadFromFile("sprites/sound_off.png"); }
	spriteSound.setTexture(textureSound);
}

void GameControl::setUsername(sf::RenderWindow& window)
{
	sf::Event event;
	sf::Text startButton;
	sf::Font font;
	sf::Texture backgroundTexture;
	sf::Sprite background;
	sf::Image icon;
	sf::Cursor cursor;
	sf::RenderWindow usernameWindow(sf::VideoMode(menuWidth, menuHeight / (ratio / 16)), "You Win!", sf::Style::Close);

	InputBar usernameBar(256, 32, 32, 32, "Username: ");
	std::string input;

	icon.loadFromFile("sprites/smiley_win.png");
	usernameWindow.setIcon(18, 18, icon.getPixelsPtr());
	font.loadFromFile("fonts\\DigifaceRegular.ttf");

	startButton.setFont(font);
	startButton.setFillColor(sf::Color(255, 60, 0));
	startButton.setOutlineColor(sf::Color::Black);
	startButton.setOutlineThickness(1.5);
	startButton.setCharacterSize(32);
	startButton.setString("OK");
	startButton.setPosition(256.f, 48.f);

	while (usernameWindow.isOpen())
	{
		while (usernameWindow.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				usernameWindow.close();
			}
			if (event.type == sf::Event::TextEntered)
			{
				usernameBar.setInputText(usernameWindow, event, 10);

				if (usernameBar.mouseOverBox(usernameWindow)) { m_sound.playPlaceFlag(); }
			}

			sf::Vector2i mouse = sf::Mouse::getPosition(usernameWindow);
			if (startButton.getGlobalBounds().contains((float)mouse.x, (float)mouse.y))
			{
				startButton.setPosition(254.f, 46.f);
				startButton.setFillColor(sf::Color(150, 255, 0));
				startButton.setScale(1.1f, 1.1f);

				if (event.type == sf::Event::MouseButtonPressed)
				{
					m_sound.playClickCell();
					m_userName = usernameBar.getInputText().getString().substring(10);
					usernameWindow.close();
				}
			}
			else {
				startButton.setPosition(256.f, 48.f);
				startButton.setScale(1, 1);
				startButton.setFillColor(sf::Color(255, 50, 0));
			}

			if (usernameBar.getInputText().getGlobalBounds().contains((float)mouse.x, (float)mouse.y))
			{
				cursor.loadFromSystem(sf::Cursor::Hand);
			}
			else
			{
				cursor.loadFromSystem(sf::Cursor::Arrow);
			}
			usernameWindow.setMouseCursor(cursor);
		}

		usernameWindow.clear(sf::Color::White);
		usernameWindow.draw(usernameBar.getInputText());
		usernameWindow.draw(startButton);
		usernameBar.mouseOverBox(usernameWindow);
		usernameWindow.display();

		window.clear(sf::Color(192, 192, 192));
		paintGameComponents(window);
		window.display();
	}
}

void GameControl::showTimer(sf::RenderWindow& window)
{
	sf::Font font;
	sf::Text timerText;
	auto foo = [&]() {
		std::string zero = "0";
		font.loadFromFile("fonts\\DigifaceRegular.ttf");
		timerText.setFont(font);

		if (!firstClick && !gameOver)
		{
			elapsed = clock.getElapsedTime();
		}

		if (elapsed.asSeconds() < 999)
		{
			if ((int)elapsed.asSeconds() >= 100)
			{
				timerText.setString(std::to_string((int)elapsed.asSeconds()));
			}
			else if ((int)elapsed.asSeconds() < 10)
			{
				timerText.setString(zero.append(zero).append((std::to_string((int)elapsed.asSeconds()))));
			}
			else
			{
				timerText.setString(zero.append(std::to_string((int)elapsed.asSeconds())));
			}
		}
		else
		{
			timerText.setString("999");
		}

		timerText.setCharacterSize(50);
		timerText.setPosition(sf::Vector2f(windowWidth - ratio * 2.5f - 10, ratio - 20));
		timerText.setFillColor(sf::Color(255, 60, 0));
		timerText.setOutlineColor(sf::Color(25, 25, 25));
		timerText.setOutlineThickness(3);
	};

	std::thread timerThread(foo);
	timerThread.join();

	window.draw(timerText);
}

void GameControl::showFlagCounter(sf::RenderWindow& window)
{
	sf::Font font;
	sf::Text flagCounter;
	font.loadFromFile("fonts\\DigifaceRegular.ttf");
	flagCounter.setFont(font);
	std::string zero = "0";
	std::string minus = "-";

	if (m_board.getFlagCounter() >= 100)
	{
		flagCounter.setString(std::to_string(m_board.getFlagCounter()));
	}
	else if (m_board.getFlagCounter() >= 0 && m_board.getFlagCounter() < 10)
	{
		flagCounter.setString(zero.append(zero).append(std::to_string(m_board.getFlagCounter())));
	}
	else if (m_board.getFlagCounter() >= 10 && m_board.getFlagCounter() <= 99)
	{
		flagCounter.setString(zero.append(std::to_string(m_board.getFlagCounter())));
	}

	else if (m_board.getFlagCounter() < 0)
	{
		if (m_board.getFlagCounter() % 100 > -10)
		{
			flagCounter.setString(minus.append(zero).append(std::to_string(-m_board.getFlagCounter() % 100)));
		}
		else if (m_board.getFlagCounter() % 100 >= -99 && m_board.getFlagCounter() % 100 <= -10)
		{
			flagCounter.setString(minus.append(std::to_string(-m_board.getFlagCounter() % 100)));
		}
	}

	flagCounter.setCharacterSize(50);
	flagCounter.setPosition(sf::Vector2f(ratio / 5 + 10, ratio - 20));
	flagCounter.setFillColor(sf::Color(255, 60, 0));
	flagCounter.setOutlineColor(sf::Color(25, 25, 25));
	flagCounter.setOutlineThickness(3);
	window.draw(flagCounter);
}

const bool GameControl::isContainedHUD(sf::Vector2f coordinates, sf::Window& window, sf::Mouse& mouse, const int offset) const
{
	return (mouse.getPosition(window).x >= coordinates.x && mouse.getPosition(window).x <= coordinates.x + ratio + offset &&
		mouse.getPosition(window).y >= coordinates.y && mouse.getPosition(window).y <= coordinates.y + ratio + offset);
}

const bool GameControl::isContainedCell(sf::Window& window, sf::Mouse& mouse) const
{
	return mouse.getPosition(window).x / ratio >= 0 && (mouse.getPosition(window).y - ratio * 4) / ratio >= 0 &&
		mouse.getPosition(window).x / ratio < m_board.getColumn() && (mouse.getPosition(window).y - ratio * 4) / ratio < m_board.getRow();
}

void GameControl::scoreThread(int recordTime)
{
	auto updateScore = [&]() {
		m_score.setScore({ m_userName, m_difficultyName, recordTime });
		m_score.writeScore(); };

	std::thread scoreThread(updateScore);
	scoreThread.join();
}

void GameControl::soundGameOver(sf::RenderWindow& window, bool& winner)
{
	isSfxPlayed = true;
	if (!winner) { m_sound.playMineDeploy(); }
	else { m_sound.playWinner(); }
}

int const GameControl::mouseOverButton(sf::Text& start, sf::Text& settings, sf::Text& exit, sf::RenderWindow& window) const
{
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		sf::Vector2i mousePos = sf::Mouse::getPosition(window);
		if (start.getGlobalBounds().contains((float)mousePos.x, (float)mousePos.y))
		{
			return 0;
		}
		if (exit.getGlobalBounds().contains((float)mousePos.x, (float)mousePos.y))
		{
			return 1;
		}
		if (settings.getGlobalBounds().contains((float)mousePos.x, (float)mousePos.y))
		{
			return 2;
		}
	}
	return -1;
}