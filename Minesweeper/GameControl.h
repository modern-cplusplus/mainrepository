#pragma once
#include "Board.h"
#include "SoundControl.h"
#include "InputBar.h"
#include "TileMap.h"
#include "Score.h"
#include <SFML/Graphics.hpp>

class GameControl
{
private:
	Board m_board;
	TileMap m_map;
	std::vector<std::unique_ptr<int>> m_cellTilesetValues;
	SoundControl m_sound;
	Score m_score = Score("score.txt");

	static constexpr size_t ratio = 32;
	static constexpr size_t menuHeight = 228;
	static constexpr size_t menuWidth = 320;

	static constexpr int beginnerHeight = 9;
	static constexpr int intermeditateHeight = 16;
	static constexpr int expertHeight = 16;
	static constexpr int beginnerWidth = 9;
	static constexpr int intermediateWidth = 16;
	static constexpr int expertWidth = 30;
	static constexpr int beginnerMineNum = 10;
	static constexpr int intermediateMineNum = 40;
	static constexpr int expertMineNum = 99;

	size_t windowWidth;
	size_t windowHeight;

	std::string m_difficultyName;
	std::string m_userName;

	sf::Clock clock;
	sf::Time elapsed;
	sf::Texture textureSmiley;
	sf::Sprite spriteSmiley;
	sf::Texture textureSound;
	sf::Sprite spriteSound;
	sf::Texture textureSize;
	sf::Sprite spriteSize;

	bool isSfxPlayed;
	bool isSfxActive;
	bool firstClick;
	bool gameOver;
	bool winner;
	bool enteredName;

	void runInitialize(sf::RenderWindow& window);

	void tilesetInitialize();
	void tilesetIndexing();
	void eventHandling(sf::RenderWindow& window);
	void settingsWindow(sf::RenderWindow& window);
	void paintGameComponents(sf::RenderWindow& window);
	void resetGame();

	void setSmiley(const std::string& filePath);
	void setCustomBoard();
	void setSound();
	void setUsername(sf::RenderWindow& window);

	void showTimer(sf::RenderWindow& window);
	void showFlagCounter(sf::RenderWindow& window);

	const bool isContainedHUD(sf::Vector2f coordinates, sf::Window& window, sf::Mouse& mouse, const int offset) const;
	const bool isContainedCell(sf::Window& window, sf::Mouse& mouse) const;

	void scoreThread(int recordTime);
	void soundGameOver(sf::RenderWindow& window, bool& winner);
	const int mouseOverButton(sf::Text& start, sf::Text& settings, sf::Text& exit, sf::RenderWindow& window) const;

public:
	GameControl();

	void run();
};