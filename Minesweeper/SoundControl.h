#pragma once
#include <SFML/Audio.hpp>
#include <memory>

class SoundControl
{
private:
	std::unique_ptr<sf::SoundBuffer> m_clickCell = std::make_unique <sf::SoundBuffer>();
	std::unique_ptr<sf::SoundBuffer> m_placeFlag = std::make_unique <sf::SoundBuffer>();
	std::unique_ptr<sf::SoundBuffer> m_mineDeploy = std::make_unique <sf::SoundBuffer>();
	std::unique_ptr<sf::SoundBuffer> m_winner = std::make_unique <sf::SoundBuffer>();
	std::unique_ptr<sf::SoundBuffer> m_easterEgg = std::make_unique <sf::SoundBuffer>();
	std::unique_ptr<sf::Sound> m_sfx = std::make_unique <sf::Sound>();

public:
	SoundControl();

	void playClickCell();
	void playPlaceFlag();
	void playMineDeploy();
	void playWinner();
	void playEasterEgg();

	void setVolume(bool isSfxActive);
};