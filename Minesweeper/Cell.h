#pragma once
#include <memory>

class Cell
{
private:
	bool m_hidden;
	bool m_flagged;
	bool m_pressed;

public:
	enum class CellState : uint8_t
	{
		HIDDEN,
		MINE,
		MINE_CLICKED,
		MINE_CROSSED,
		FLAG,
		EMPTY,
		NUMBER_1,
		NUMBER_2,
		NUMBER_3,
		NUMBER_4,
		NUMBER_5,
		NUMBER_6,
		NUMBER_7,
		NUMBER_8
	};

	Cell();
	Cell(CellState state);

	const CellState getState() const;

	void setState(CellState state);

	bool isHidden();
	bool isFlagged();
	bool isPressed();

	void reveal();
	void flagging();
	void pressing();
	void unpressing();

private:
	CellState m_state;
};