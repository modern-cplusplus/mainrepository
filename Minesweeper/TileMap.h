#pragma once
#include <SFML/Graphics.hpp>
#include <set>

class TileMap : public sf::Drawable, public sf::Transformable
{
private:
	sf::VertexArray m_vertices;
	sf::Texture m_tileset;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

public:
	bool load(const std::string& tileset,
		sf::Vector2u tileSize,
		const std::vector<std::unique_ptr<int>> tiles,
		size_t columns,
		size_t rows,
		std::set<std::pair<size_t, size_t>> modifiedCells);
};