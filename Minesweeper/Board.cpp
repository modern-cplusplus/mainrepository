#include "Board.h"
#include <random>
#include <queue>

Board::Board() :
	m_row(16),
	m_column(30),
	m_mineNumber(99),
	m_flagCounter((short int)m_mineNumber),
	m_cellsRevealed(m_row* m_column - m_mineNumber)
{
	generateStartingBoard();
}

Board::Board(const size_t& row, const size_t& column, const size_t& mineNumber) :
	m_row(row),
	m_column(column),
	m_mineNumber(mineNumber),
	m_flagCounter((short int)m_mineNumber),
	m_cellsRevealed(m_row* m_column - m_mineNumber)
{
	generateStartingBoard();
}

std::vector<std::vector<Cell>> Board::getBoard() const
{
	return m_board;
}

const size_t Board::getRow() const
{
	return m_row;
}

const size_t Board::getColumn() const
{
	return m_column;
}

const size_t Board::getMineNumber() const
{
	return m_mineNumber;
}

const size_t Board::getCellsRevealed()const
{
	return m_cellsRevealed;
}

const short int Board::getFlagCounter() const
{
	return m_flagCounter;
}

const std::set<std::pair<size_t, size_t>> Board::getModifiedCells()const
{
	return m_modifiedCells;
}

void Board::setFlagCounter(short int flagCounter)
{
	m_flagCounter = flagCounter;
}

void Board::generateStartingBoard()
{
	for (size_t index = 0; index < m_row; ++index)
	{
		std::vector<Cell> vectorCell;
		m_board.emplace_back(vectorCell);

		for (size_t index2 = 0; index2 < m_column; ++index2)
		{
			Cell cell(Cell::CellState::EMPTY);
			m_board[index].emplace_back(cell);

			m_modifiedCells.insert({ index, index2 });
		}
	}
}

void Board::generateBoardAfterClick(const size_t& clickedRow, const size_t& clickedCol)
{
	deployMines(clickedRow, clickedCol);
	markNumberMineNeighbors();
	clickCell(clickedRow, clickedCol);
}

void Board::deployMines(const size_t& clickedRow, const size_t& clickedCol)
{
	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	std::uniform_int_distribution<int> distribution(0, m_row * m_column - 1);

	size_t minesRemaining = m_mineNumber;

	while (minesRemaining > 0)
	{
		int deployment = distribution(generator);
		short int chosenRow = deployment / (short int)m_column;
		short int chosenCol = deployment % (short int)m_column;

		while ((chosenRow >= (short int)clickedRow - 1 && chosenRow <= (short int)clickedRow + 1 &&
			chosenCol >= (short int)clickedCol - 1 && chosenCol <= (short int)clickedCol + 1) ||
			(m_board[chosenRow][chosenCol].getState() == Cell::CellState::MINE))
		{
			deployment = distribution(generator);
			chosenRow = deployment / (short int)m_column;
			chosenCol = deployment % (short int)m_column;
		}

		m_board[chosenRow][chosenCol].setState(Cell::CellState::MINE);
		m_mineCoordinates.insert({ chosenRow, chosenCol });
		m_unflaggedMines.insert({ chosenRow, chosenCol });
		if (m_wrongFlagsCoordinates.count({ chosenRow, chosenCol }) != 0)
		{
			m_wrongFlagsCoordinates.erase({ chosenRow, chosenCol });
		}

		--minesRemaining;
	}
}

void Board::markNumberMineNeighbors()
{
	for (size_t rowIndex = 0; rowIndex < m_row; ++rowIndex)
	{
		for (size_t colIndex = 0; colIndex < m_column; ++colIndex)
		{
			if (m_board[rowIndex][colIndex].getState() != Cell::CellState::MINE)
			{
				uint8_t mineTotal = 0;

				for (short int rowOff = -1; rowOff <= 1; ++rowOff)
				{
					for (short int colOff = -1; colOff <= 1; ++colOff)
					{
						short int currentRow = (short int)rowIndex + rowOff;
						short int currentCol = (short int)colIndex + colOff;

						if (currentRow > -1 && currentRow < (short int)m_row && currentCol > -1 && currentCol < (short int)m_column)
						{
							Cell::CellState currentCellState = m_board[currentRow][currentCol].getState();

							if (currentCellState == Cell::CellState::MINE)
							{
								++mineTotal;
							}
						}
					}
				}

				switch (mineTotal)
				{
				case 0:
					m_board[rowIndex][colIndex].setState(Cell::CellState::EMPTY);
					break;

				case 1:
					m_board[rowIndex][colIndex].setState(Cell::CellState::NUMBER_1);
					break;

				case 2:
					m_board[rowIndex][colIndex].setState(Cell::CellState::NUMBER_2);
					break;

				case 3:
					m_board[rowIndex][colIndex].setState(Cell::CellState::NUMBER_3);
					break;

				case 4:
					m_board[rowIndex][colIndex].setState(Cell::CellState::NUMBER_4);
					break;

				case 5:
					m_board[rowIndex][colIndex].setState(Cell::CellState::NUMBER_5);
					break;

				case 6:
					m_board[rowIndex][colIndex].setState(Cell::CellState::NUMBER_6);
					break;

				case 7:
					m_board[rowIndex][colIndex].setState(Cell::CellState::NUMBER_7);
					break;

				case 8:
					m_board[rowIndex][colIndex].setState(Cell::CellState::NUMBER_8);
					break;
				}
			}
		}
	}
}

void Board::insertModifiedCells(std::pair<size_t, size_t> coordinates)
{
	m_modifiedCells.insert(coordinates);
}

void Board::clearModifiedCells()
{
	m_modifiedCells.clear();
}

const bool Board::validateDifficulty(const size_t& row, const size_t& column, const size_t& mineNumber) const
{
	if (row >= default_row_min && row <= default_row_max
		&& column >= default_column_min && column <= default_column_max
		&& mineNumber >= default_mineNumber && mineNumber <= (row - 1) * (column - 1))
	{
		return true;
	}

	return false;
}

void Board::fullMineReveal(size_t clickedRow, size_t clickedCol)
{
	for (const auto& itr : m_mineCoordinates)
	{
		m_board[itr.first][itr.second].reveal();
		m_modifiedCells.insert({ itr.first, itr.second });
	}
	for (const auto& itr : m_wrongFlagsCoordinates)
	{
		m_board[itr.first][itr.second].flagging();
		m_board[itr.first][itr.second].reveal();
		m_board[itr.first][itr.second].setState(Cell::CellState::MINE_CROSSED);
		m_modifiedCells.insert({ itr.first, itr.second });
	}
	m_board[clickedRow][clickedCol].setState(Cell::CellState::MINE_CLICKED);
}

void Board::fullMineFlag()
{
	for (const auto& itr : m_unflaggedMines)
	{
		m_board[itr.first][itr.second].flagging();
		m_modifiedCells.insert({ itr.first, itr.second });
	}
}

void Board::clickCell(const size_t& clickedRow, const size_t& clickedCol)
{
	if (!m_board[clickedRow][clickedCol].isHidden() || m_board[clickedRow][clickedCol].isFlagged())
	{
		return;
	}

	if (m_board[clickedRow][clickedCol].getState() == Cell::CellState::MINE)
	{
		fullMineReveal(clickedRow, clickedCol);
		return;
	}

	if (m_board[clickedRow][clickedCol].getState() >= Cell::CellState::NUMBER_1 && m_board[clickedRow][clickedCol].getState() <= Cell::CellState::NUMBER_8)
	{
		if (m_board[clickedRow][clickedCol].isHidden())
		{
			m_board[clickedRow][clickedCol].reveal();
			m_modifiedCells.insert({ clickedRow, clickedCol });
			--m_cellsRevealed;
		}
		return;
	}

	std::queue<std::pair<size_t, size_t>> neighbourQueue;
	neighbourQueue.push({ clickedRow, clickedCol });

	while (!neighbourQueue.empty())
	{
		for (short int rowOff = -1; rowOff <= 1; ++rowOff)
		{
			for (short int colOff = -1; colOff <= 1; ++colOff)
			{
				short int currentRow = (short int)neighbourQueue.front().first + rowOff;
				short int currentCol = (short int)neighbourQueue.front().second + colOff;

				if (currentRow > -1 && currentRow < (short int)m_row && currentCol > -1 && currentCol < (short int)m_column)
				{
					Cell::CellState currentCellState = m_board[currentRow][currentCol].getState();

					if (m_board[currentRow][currentCol].isHidden())
					{
						if (currentCellState == Cell::CellState::EMPTY)
						{
							neighbourQueue.push({ currentRow, currentCol });
							m_board[currentRow][currentCol].reveal();
						}
						else if (currentCellState >= Cell::CellState::NUMBER_1 && currentCellState <= Cell::CellState::NUMBER_8)
						{
							m_board[currentRow][currentCol].reveal();
						}
						m_modifiedCells.insert({ currentRow, currentCol });
						--m_cellsRevealed;
					}
				}
			}
		}
		neighbourQueue.pop();
	}
}

void Board::doFlagging(const size_t& clickedRow, const size_t& clickedCol)
{
	if (!m_board[clickedRow][clickedCol].isFlagged() && m_board[clickedRow][clickedCol].isHidden())
	{
		--m_flagCounter;
	}
	else if (m_board[clickedRow][clickedCol].isFlagged())
	{
		++m_flagCounter;
	}

	if (m_board[clickedRow][clickedCol].isFlagged() || m_board[clickedRow][clickedCol].isHidden())
	{
		m_board[clickedRow][clickedCol].flagging();
		m_modifiedCells.insert({ clickedRow, clickedCol });

		if (m_board[clickedRow][clickedCol].getState() != Cell::CellState::MINE && m_board[clickedRow][clickedCol].isFlagged())
		{
			m_wrongFlagsCoordinates.insert({ clickedRow, clickedCol });
		}
		else
		{
			m_wrongFlagsCoordinates.erase({ clickedRow, clickedCol });
		}

		if (m_board[clickedRow][clickedCol].getState() == Cell::CellState::MINE && !m_board[clickedRow][clickedCol].isFlagged())
		{
			m_unflaggedMines.insert({ clickedRow, clickedCol });
		}
		else
		{
			m_unflaggedMines.erase({ clickedRow, clickedCol });
		}
	}
}

void Board::doPressing(const size_t& clickedRow, const size_t& clickedCol)
{
	m_board[clickedRow][clickedCol].pressing();
}

void Board::doUnpressing(const size_t& clickedRow, const size_t& clickedCol)
{
	m_board[clickedRow][clickedCol].unpressing();
}

Board::~Board()
{
	m_board.clear();
}