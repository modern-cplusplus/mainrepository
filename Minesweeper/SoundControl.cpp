#include "SoundControl.h"

SoundControl::SoundControl()
{
	m_clickCell->loadFromFile("sound/clickCell.ogg");
	m_placeFlag->loadFromFile("sound/placeFlag.ogg");
	m_mineDeploy->loadFromFile("sound/mineDeploy.ogg");
	m_winner->loadFromFile("sound/winner.ogg");
	m_easterEgg->loadFromFile("sound/easterEgg.ogg");
}

void SoundControl::playClickCell()
{
	m_sfx->setBuffer(*m_clickCell);
	m_sfx->play();
}

void SoundControl::playPlaceFlag()
{
	m_sfx->setBuffer(*m_placeFlag);
	m_sfx->play();
}

void SoundControl::playMineDeploy()
{
	m_sfx->setBuffer(*m_mineDeploy);
	m_sfx->play();
}

void SoundControl::playWinner()
{
	m_sfx->setBuffer(*m_winner);
	m_sfx->play();
}

void SoundControl::playEasterEgg()
{
	m_sfx->setBuffer(*m_easterEgg);
	m_sfx->play();
}

void SoundControl::setVolume(bool isSfxActive)
{
	if (isSfxActive) { m_sfx->setVolume(0); }
	else { m_sfx->setVolume(100); }
}