#include "Cell.h"

Cell::Cell() :
	m_state(CellState::HIDDEN),
	m_hidden(true),
	m_flagged(false),
	m_pressed(false)
{
}

Cell::Cell(CellState state) :
	m_state(state),
	m_hidden(true),
	m_flagged(false),
	m_pressed(false)
{
}

const Cell::CellState Cell::getState() const
{
	return m_state;
}

void Cell::setState(CellState state)
{
	m_state = state;
}

bool Cell::isHidden()
{
	return m_hidden;
}

bool Cell::isFlagged()
{
	return m_flagged;
}

bool Cell::isPressed()
{
	return m_pressed;
}

void Cell::reveal()
{
	m_hidden = false;
}

void Cell::flagging()
{
	if (!m_flagged && m_hidden)
	{
		m_flagged = true;
		m_hidden = false;
		return;
	}
	m_flagged = false;
	m_hidden = true;
}

void Cell::pressing()
{
	m_pressed = true;
}

void Cell::unpressing()
{
	m_pressed = false;
}